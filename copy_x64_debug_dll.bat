@ECHO OFF

IF DEFINED ProgramW6432 (
SET SPDDIR="%ProgramW6432%\SP Devices"
) ELSE (
SET SPDDIR="%ProgramFiles%\SP Devices"
)

REM Remove previous adqapi.h to make sure there will not be mixing between old adqapi.h and new adqapi.dll
IF EXIST adqapi.h del adqapi.h

IF EXIST ..\x64\Debug\adqapi.dll (GOTO COPY_DEV_FILES)
IF EXIST %SPDDIR%\ADQAPI_x64\adqapi.dll (GOTO COPY_INSTALLED_FILES)
GOTO ERROR

:COPY_DEV_FILES
ECHO Copying development x64 debug ADQAPI...
copy ..\x64\Debug\adqapi.dll .\x64\Debug\
IF NOT ERRORLEVEL 0 GOTO ERROR
copy ..\x64\Debug\adqapi.lib .\x64\Debug\
IF NOT ERRORLEVEL 0 GOTO ERROR
copy ..\Release\adqapi.h .
IF NOT ERRORLEVEL 0 GOTO ERROR
GOTO END

:COPY_INSTALLED_FILES
ECHO Copying installed x64 debug ADQAPI...
copy %SPDDIR%\ADQAPI_x64\adqapi.dll .\x64\Debug\
IF NOT ERRORLEVEL 0 GOTO ERROR
copy %SPDDIR%\ADQAPI_x64\adqapi.lib .\x64\Debug\
IF NOT ERRORLEVEL 0 GOTO ERROR
copy %SPDDIR%\ADQAPI_x64\adqapi.h .
IF NOT ERRORLEVEL 0 GOTO ERROR
GOTO END

:ERROR
ECHO Error, missing ADQAPI files!
EXIT /B 1

:END
ECHO ...done!
EXIT /B 0
