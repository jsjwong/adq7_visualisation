// File: ADQ_simple_example.cpp
// Description: A simple example of how to use the ADQAPI.
// This example is generic for all SP Devices Data Acquisition Boards (ADQs)
// The example sets some basic settings and collects data.

#define _CRT_SECURE_NO_WARNINGS // This define removes warnings for printf

#include "Dependencies\glew\glew.h"
#include "Dependencies\freeglut\freeglut.h"

#include "ADQAPI.h"
#include "os.h"
#include <stdio.h>
#include <stdint.h>
#include <windows.h>
#include <conio.h>
#include <time.h>
#include <stdlib.h>

//#define VERBOSE

//#ifdef LINUX
//    #include <stdlib.h>
//    #include <string.h>
//    #include <unistd.h>
//    #define Sleep(interval) usleep(1000*interval)
//    #define POLLING_FOR_DATA //Must be defined for PCIe devices under Linux
//#endif

//#define CHECKADQ(f) if(!(f)){printf("\nError in " #f "\n"); if (WithinGLUTloop) glutLeaveMainLoop(); else goto error;}
#define CHECKADQ(f) if(!(f)){printf("\nError in " #f "\n"); goto error;}

#define MIN(a,b) ((a) > (b) ? (b) : (a))
inline void min16(GLshort &a, GLshort b) { if (b < a) a = b; }
#define MAX(a,b) ((a) > (b) ? (a) : (b))
inline void max16(GLshort &a, GLshort b) { if (b > a) a = b; }

const double KiMulitplier = 1.0 / 1024;
const double MiMulitplier = 1.0 / (1024 * 1024);
const double GiMulitplier = 1.0 / (1024 * 1024 * 1024);
#define Byte2KiB(b) (b*KiMulitplier)
#define Byte2MiB(b) (b*MiMulitplier)
#define Byte2GiB(b) (b*GiMulitplier)

void adq7_triggered_streaming(void *adq_cu, int adq_num, int *argc, char **argv);

bool file_exists(const char *fname)
{
    FILE *file;
    if ((file = fopen(fname, "r")))
    {
        fclose(file);
        return true;
    }
    return false;
}

int ConsolePause (const char* Message) {
	while (_kbhit()) { //flush all keyboard events queued up in the buffer
		_getch();
	}
	printf("\n%s\n",Message);
	return _getch(); //For MS Windows
}

HMENU hmenu;
void adq7(void *adq_cu, int adq_num, int *argc, char **argv)
{

	HWND hwnd = GetConsoleWindow();
	hmenu = GetSystemMenu(hwnd, FALSE);
	
	//SetConsoleMode(hwnd,ENABLE_PROCESSED_INPUT);

	WCHAR consoleTitle[256];
	wsprintf(consoleTitle, _T("ADQ7 CCTV Console"));
	SetConsoleTitle((LPWSTR)consoleTitle);

	char *serialnumber = ADQ_GetBoardSerialNumber(adq_cu, adq_num);
	int* revision = ADQ_GetRevision(adq_cu, adq_num);
	double fs = 0.0;
	unsigned int nofchannels = ADQ_GetNofChannels(adq_cu, adq_num);
	ADQ_GetSampleRate(adq_cu, adq_num, 0, &fs);

	printf("\nConnected to ADQ7 #1\n\n");

	printf("Device Serial Number: %s\n", serialnumber);
	printf("Firmware Revision: %d\n", revision[0]);
	printf("%u channels, %.2f GSPs\n", nofchannels, fs / 1000 / 1000 / 1000);

	//unsigned int tlocal = ADQ_GetTemperature(adq_cu, adq_num, 0) / 256;
	//unsigned int tr1 = ADQ_GetTemperature(adq_cu, adq_num, 1) / 256;
	//unsigned int tr2 = ADQ_GetTemperature(adq_cu, adq_num, 2) / 256;
	//unsigned int tr3 = ADQ_GetTemperature(adq_cu, adq_num, 3) / 256;
	//unsigned int tr4 = ADQ_GetTemperature(adq_cu, adq_num, 4) / 256;
	//printf("Temperatures:\n\tLocal: %u\n\tADC0: %u\n\tADC1: %u\n\tFPGA: %u\n\tPCB diode: %u\n\n", tlocal, tr1, tr2, tr3, tr4);

	// //mode = 0;
	while (1) {
		EnableMenuItem(hmenu, SC_CLOSE, MF_GRAYED); //Stop user from closing the console window
		adq7_triggered_streaming(adq_cu, adq_num, argc, argv);
		EnableMenuItem(hmenu, SC_CLOSE, MF_ENABLED); //enable user from closing the console window
		ConsolePause ("Press Any Key to Restart Again.");
		printf("\n");

	}
}


// OpenGL Functions prototypes and Global variables *******************************
typedef struct {
	int Min;
	int Max;
	int Range;
	float Scale;
} ImgInfo_t;

static BOOL WINAPI console_ctrl_handler(DWORD dwCtrlType); // Handler function will be called on separate thread!

void GL_main(int *argc, char **argv);
void SetVSync(BOOL sync);

void GL_reshape(int w, int h);
void init_tex(void);
void draw_tex(void);

void SpecialInputDn(int key, int x, int y);
void SpecialInputUp(int key, int x, int y);
void keyboard(unsigned char key, int x, int y);
void CloseFn(void);

ImgInfo_t GetImageScale(int target_range, int max_margin, int min_margin, bool UsePrev, bool verbose);

#define KEY_SHIFT 112
#define KEY_CTRL 114
bool shift_key_dn = false;
bool ctrl_key_dn = false;
bool TriggerPosChanged = false;

GLubyte TrigCoarseTune = 0;
GLubyte TrigFineTune = 0; 
GLuint TrigShift = 0;

int vsync = 1;      // Used as a boolean (1 or 0) for "on" and "off"
int RunMode = 1;      // Used as a boolean (1 or 0) for "on" and "off"
bool WithinGLUTloop = false; //for handling ADQ errors while within the GLUT main loop

unsigned int FrameCount = 0;

// *** Hardware constants(do not change!)  *********************************************
const unsigned char SampleBlockSize = 32; //Parallel sample block size

//*****Available Systems*********************
#define Target_FACED 0

//************* System Specific Data Acquisition and Image Segmentation Parameters *****************
#define TARGET_SYSTEM 0 // Choose target system here;


//**************************
#if	TARGET_SYSTEM == Target_FACED //***************************************
#define REC_LINE_LEN 512 //Must be multiples of 32
#define REC_LINE_NUM 3000
int analog_bias = 0; // Bias ADC analog signal
bool InvertSignal = false; //Signal inversion
const double Laser_Clock = 20; //MHz;
const double Target_ADC_Clock = 2560.00; //MHz;
unsigned int CellDetectTh = 2200; //DetectThresholdWidth = 15 bits;
unsigned int Diff_X_offset = 40; //LineDelayCntWidth = 6 bits;
unsigned int CellMargin = 40; //MarginLineNumWidth = 7 bits;
const char SysName[] = "_FACED";
#else //Default settings ***************************************
#define REC_LINE_LEN 832 //Must be multiples of 32
#define REC_LINE_NUM 2000
int analog_bias = 0; // Bias ADC analog signal
bool InvertSignal = false; //Signal inversion
const double Target_ADC_Clock = 2464.80; //MHz;
const double Laser_Clock = 11.85; //MHz;
unsigned int CellDetectTh = 900;//900; // 2680;//900; //DetectThresholdWidth = 15 bits;
unsigned int Diff_X_offset = 25; //LineDelayCntWidth = 6 bits;
unsigned int CellMargin = 25; //MarginLineNumWidth = 7 bits;
const char SysName[] = "_UNKNOWN";
#endif

#ifdef PEAK_DIST
	const GLubyte PeakDist = PEAK_DIST;
#else
	const GLubyte PeakDist = 10; //default value
#endif

//************* Cell trigger parameters **************************************
unsigned int IncludeLastCellNext = 0;
unsigned int BackgroundSeg = 0;
unsigned int UseCellDetect = 1;
unsigned int UseDelayedStart = 1;
unsigned int DebugDetect = 0;
unsigned int EnableSegmentation = 0;
unsigned int UL2_Mode = 2; // 2;
bool CellParamChanged = false;
unsigned int CellParam = IncludeLastCellNext << 10 | BackgroundSeg << 9 | UseCellDetect << 8 | UseDelayedStart << 7 | DebugDetect << 6 | EnableSegmentation << 5 | UL2_Mode << 1;
bool SegParamChanged = false;
unsigned int SegmentationParam = CellDetectTh << 16 | Diff_X_offset << 8 | CellMargin;
//****************************************************************************

const unsigned int Quarter_Line_Len = (unsigned int)(0.5 + Target_ADC_Clock / Laser_Clock);
const double Actual_ADC_Clock = Laser_Clock * Quarter_Line_Len;
const double FPGA_Clock = Actual_ADC_Clock * 4 / SampleBlockSize;

unsigned int CellCountPeriod = ((unsigned int)1) << 22; //cell count accumulation period in terms of the number of lines passed.
double LinesPerSec = Laser_Clock * 1e6;
double CellCountConversionFactor = LinesPerSec / CellCountPeriod;


//*******Cell segmentation offset and margin limits**************************************************************
const int MaxCellMargin = 255;
const int StandardLineLen = 1280;
const int StandardDiffOffsetNum = 96;

const int AbsoluteMaxDiffOffset = ((StandardLineLen/32)*StandardDiffOffsetNum)/(REC_LINE_LEN/32);
const int MaxDiffOffsetCtrlLimit = 256;
const int MaxDiffOffset = (AbsoluteMaxDiffOffset > MaxDiffOffsetCtrlLimit) ? MaxDiffOffsetCtrlLimit : AbsoluteMaxDiffOffset;


#define ImageWidth REC_LINE_LEN //256*2
#define ImageHeight REC_LINE_NUM
size_t ImageSampNum; //#define ImageSampNum (ImageHeight*ImageWidth) //REC_LINE_NUM*REC_LINE_LEN
size_t TotalSampNum = 0;

//Texture is rotated 90 degrees clockwise, so frame width = texture height, and frame height = texture width
#define FrameWidth REC_LINE_NUM
#define FrameHeight REC_LINE_LEN

unsigned int TopCrop = 0;

GLshort mono16Image1D[ImageHeight*(ImageWidth+1)];
GLshort mono16ImageBGLine[ImageWidth]={0};
GLshort mono16ImageBGLineH[ImageHeight]={0};
GLubyte Scaled8Image1D[ImageHeight*ImageWidth];
GLubyte Scaled8Image2D[ImageHeight][ImageWidth];

size_t buffer_cnt = 0;
GLshort SaveBuffer[5000* REC_LINE_LEN] = { 0 };

// ********************************************************************************

typedef struct {
	void *cu;
	int num;
} ADQ_t;
ADQ_t adq_par;

typedef struct
{
  unsigned char RecordStatus;
  unsigned char UserID;
  unsigned char Channel;
  unsigned char DataFormat;
  unsigned int SerialNumber;
  unsigned int RecordNumber;
  unsigned int SamplePeriod;
  unsigned long long Timestamp;
  unsigned long long RecordStart;
  unsigned int RecordLength;
  unsigned int Reserved;
} StreamingHeader_t; //40 bytes


void adq7_triggered_streaming_file_writer(//unsigned int write_to_file,
	short* data_buffer, unsigned int samples_to_write, FILE* file)
{
	memcpy(&SaveBuffer[buffer_cnt], data_buffer, sizeof(short)*samples_to_write);
	buffer_cnt += samples_to_write;
}


#define CH_NUM 2

// Buffers to handle the stream output (both headers and data)
short* target_buffers[4] = { NULL, NULL, NULL, NULL };
short* target_buffers_extradata[4] = { NULL, NULL, NULL, NULL };
StreamingHeader_t* target_headers[4] = { NULL, NULL, NULL, NULL };

FILE* outfile_data[4] = { NULL, NULL, NULL, NULL };
FILE* outfile_headers[4] = { NULL, NULL, NULL, NULL };

unsigned char channelsmask = 0x00;
unsigned int timeout_ms = 10;
size_t nof_records = 0;
size_t nof_records_all_ch = 0;
unsigned int samples_per_record = REC_LINE_LEN;

__int64 ctr_init = 0, freq = 0;
__int64 ctr_start = 0, ctr_finish, ctr_end = 0, ctr_prev = ctr_init; //time measurement
double period_s;
bool Start_Save = false;
bool  SHOW_STATUS_SAVING = false;
//#define SHOW_STATUS

//unsigned char channelsmask, unsigned int timeout_ms, unsigned int nof_records, unsigned int nof_records_all_ch
int AcquireData(const bool Save_Data_Enable = false) {

	void *adq_cu = adq_par.cu;
	int adq_num = adq_par.num;


	unsigned int success = 1;
	unsigned int buffers_filled = 0;
	unsigned int retval;
	unsigned int ch = 0;
	size_t nof_received_records_all_ch = 0;
	size_t records_completed[4] = { 0, 0, 0, 0 };
	bool received_all_records = 0;

	// Variables to handle the stream output (both headers and data)
	unsigned int header_status[4] = { 0, 0, 0, 0 };
	unsigned int samples_added[4] = { 0, 0, 0, 0 };
	unsigned int headers_added[4] = { 0, 0, 0, 0 };
	unsigned int samples_extradata[4] = { 0, 0, 0, 0 };
	unsigned int samples_remaining;
	unsigned int headers_done = 0;

	unsigned int TimeOutCount = 0;
	const unsigned int TimeOutCountMax = 1024; //4096;
	unsigned int Actual_Rec_Count = 0;
	bool Reached_Target_Count = false;
	size_t SampleIndex = 0;

	//

	if (CellParamChanged) {
		CellParamChanged = false;
		CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 16, 0, CellParam, &retval)); //update Cell parameter settings
	}
	if (SegParamChanged) {
		SegParamChanged = false;
		CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 17, 0, SegmentationParam, &retval)); //update cell trigger/seqmentation settings
	}


	CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 18, 0, ((unsigned int)1) << 31, &retval)); //Reset data acquisition and FPGA ine count
	CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 18, 0, nof_records, &retval));  //set number of lines(records) for data acquisition

	// Collection loop
	buffer_cnt = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&ctr_start);
	do {

		buffers_filled = 0;

		if (ADQ_GetStreamOverflow(adq_cu, adq_num)) {
			printf("\nStreaming overflow detected...\n");
			goto error;
		}

		//************** Wait for one or more transfer buffers **************************************
		TimeOutCount = 0;
		while (!buffers_filled) {
#ifdef DEBUG_BUFFER_STATUS
			printf("Waiting for transfer buffer...\n");
#endif
			CHECKADQ(ADQ_WaitForTransferBuffer(adq_cu, adq_num, &buffers_filled, timeout_ms));


#ifdef DEBUG_BUFFER_STATUS
			//CHECKADQ(ADQ_ReadUserRegister(adq_cu, adq_num, 2, 16, &Actual_Rec_Count));
			//printf("Actual_Rec_Count = %u\n", Actual_Rec_Count);
			//printf("Number of records received: %u, Buffer filled: %u\n", Actual_Rec_Count, buffers_filled);
			printf("Buffer filled: %u\n", buffers_filled);
#endif
			if (!buffers_filled) {


				if (!Reached_Target_Count) { //if (!Actual_Rec_Count) {
					CHECKADQ(ADQ_ReadUserRegister(adq_cu, adq_num, 2, 16, &Actual_Rec_Count));
#ifdef SHOW_STATUS
					printf("\nActual_Rec_Count = %u\n", Actual_Rec_Count);
#endif
				}		
#ifdef SHOW_STATUS
				printf("Timeout %u, flushing DMA...\n", TimeOutCount);
#endif
				if (Actual_Rec_Count>= nof_records) {
					ADQ_FlushDMA(adq_cu, adq_num);
					Reached_Target_Count = true;
				}
				TimeOutCount++;
				if (TimeOutCount>=4) {
					printf("Timeout count = %u, Actual_Rec_Count = %llu\n",TimeOutCount, Actual_Rec_Count);

					if (TimeOutCount >= TimeOutCountMax) {
						printf("Error: Max Timeout count reached. Actual_Rec_Count = %llu\n", Actual_Rec_Count);
						goto error;
					}

				}

			}
		}

		//**********************************************************************

		// *** Fetch the data by calling GetDataStreaming()
		//printf("Receiving data...\n");
		CHECKADQ(ADQ_GetDataStreaming(adq_cu, adq_num,
			(void**)target_buffers,
			(void**)target_headers,
			channelsmask,
			samples_added,
			headers_added,
			header_status));

		// *** Parse the data
		for (ch = 0; ch < CH_NUM; ++ch) {
			if (!((1 << ch) & channelsmask))
				continue;

			if (headers_added[ch] > 0) {
				if (header_status[ch]) {
					headers_done = headers_added[ch];
				} else {
					// One incomplete record in the buffer (header is copied to the front
					// of the buffer later)
					headers_done = headers_added[ch] - 1;
				}
				// If there is at least one complete header
				records_completed[ch] += headers_done;
			}

			// *** Parse the added samples
			if (samples_added[ch] > 0) {
				samples_remaining = samples_added[ch];
				//printf("Samples added %u for ch %u\n", samples_added[ch], ch);
				//printf("Samples added %u for ch %u, Record count: %.3f\n", samples_added[ch], ch, (float)samples_added[ch]/samples_per_record);
				//printf("Samples added %u for ch %u, Record completed: %u\n", samples_added[ch], ch, records_completed[ch]);
				//if (SHOW_STATUS_SAVING) {
				//	printf("Lines received: %u\r", records_completed[ch]);
				//}

				//printf("Completed record %u, %u samples.\n", target_headers[ch][0].RecordNumber, target_headers[ch][0].RecordLength);
				// *** Handle incomplete record at the start of the buffer
				if (samples_extradata[ch] > 0) {
					if (headers_done == 0) {
						// There is not enough data in the transfer buffer to complete
						// the record. Add all the samples to the extradata buffer.
						memcpy(&target_buffers_extradata[ch][samples_extradata[ch]],
							target_buffers[ch],
							sizeof(short)*samples_added[ch]);
						samples_remaining -= samples_added[ch];
						samples_extradata[ch] += samples_added[ch];
					}
					else {
						// There is enough data in the transfer buffer to complete
						// the record. Add RecordLength-samples_extradata samples

						if (!Save_Data_Enable) {
							memcpy(&mono16Image1D[SampleIndex], target_buffers_extradata[ch], sizeof(short)*samples_extradata[ch]);
							memcpy(&mono16Image1D[SampleIndex + samples_extradata[ch]], target_buffers[ch], sizeof(short)*(target_headers[ch][0].RecordLength - samples_extradata[ch]));
						}
						SampleIndex += target_headers[ch][0].RecordLength; // -samples_extradata[ch];
																		   //[ImageHeight*ImageWidth]

						adq7_triggered_streaming_file_writer(
							target_buffers_extradata[ch],
							samples_extradata[ch],
							outfile_data[ch]);
						adq7_triggered_streaming_file_writer(
							target_buffers[ch],
							target_headers[ch][0].RecordLength - samples_extradata[ch],
							outfile_data[ch]);
						/* if (write_header_file)
						fwrite(&target_headers[ch][0],
						sizeof(StreamingHeader_t), 1,
						outfile_headers[ch]);*/

						samples_remaining -= target_headers[ch][0].RecordLength - samples_extradata[ch];
						samples_extradata[ch] = 0;
#ifdef VERBOSE
						printf("Completed record %u on channel %u, %u samples.\n",
							target_headers[ch][0].RecordNumber, ch,
							target_headers[ch][0].RecordLength);
#endif
					}
				}
				else {
					if (headers_done == 0) {
						// The samples in the transfer buffer begin a new record, this
						// record is incomplete.
						memcpy(target_buffers_extradata[ch],
							target_buffers[ch],
							sizeof(short)*samples_added[ch]);
						samples_remaining -= samples_added[ch];
						samples_extradata[ch] = samples_added[ch];
					}
					else {
						// The samples in the transfer buffer begin a new record, this
						// record is complete.

						if (!Save_Data_Enable) memcpy(&mono16Image1D[SampleIndex], target_buffers[ch], sizeof(short)*target_headers[ch][0].RecordLength);
						SampleIndex += target_headers[ch][0].RecordLength;

						adq7_triggered_streaming_file_writer(
							target_buffers[ch],
							target_headers[ch][0].RecordLength,
							outfile_data[ch]);
						//if (write_header_file)
						//  fwrite(&target_headers[ch][0],
						//         sizeof(StreamingHeader_t), 1,
						//         outfile_headers[ch]);

						samples_remaining -= target_headers[ch][0].RecordLength;

#ifdef VERBOSE
						printf("Completed record %u on channel %u, %u samples.\n",
							target_headers[ch][0].RecordNumber, ch,
							target_headers[ch][0].RecordLength);
#endif
					}
				}
				// At this point: the first record in the transfer buffer or the entire
				// transfer buffer has been parsed.

				// *** Loop through complete records fully inside the buffer
				for (unsigned int i = 1; i < headers_done; ++i) {

					if (!Save_Data_Enable) memcpy(&mono16Image1D[SampleIndex], &target_buffers[ch][samples_added[ch] - samples_remaining], sizeof(short)*target_headers[ch][i].RecordLength);
					SampleIndex += target_headers[ch][i].RecordLength;

					adq7_triggered_streaming_file_writer(
						(&target_buffers[ch][samples_added[ch] - samples_remaining]),
						target_headers[ch][i].RecordLength,
						outfile_data[ch]);
					//if (write_header_file)
					//  fwrite(&target_headers[ch][0],
					//         sizeof(StreamingHeader_t), 1,
					//         outfile_headers[ch]);

					samples_remaining -= target_headers[ch][i].RecordLength;

#ifdef VERBOSE
					printf("Completed record %u on channel %u, %u samples.\n",
						target_headers[ch][i].RecordNumber, ch,
						target_headers[ch][i].RecordLength);
#endif
				}

				if (samples_remaining > 0) {
					// There is an incomplete record at the end of the transfer buffer
					// Copy the incomplete header to the start of the target_headers buffer
					memcpy(target_headers[ch],
						&target_headers[ch][headers_done],
						sizeof(StreamingHeader_t));

					// Copy any remaining samples to the target_buffers_extradata buffer,
					// they belong to the incomplete record
					memcpy(target_buffers_extradata[ch],
						&target_buffers[ch][samples_added[ch] - samples_remaining],
						sizeof(short)*samples_remaining);
					// printf("Incomplete at end of transfer buffer. %u samples.\n", samples_remaining);
					// printf("Copying %u samples to the extradata buffer\n", samples_remaining);
					samples_extradata[ch] = samples_remaining;
					samples_remaining = 0;
				}

			}
		}

		// *** Update received_all_records
		nof_received_records_all_ch = 0;
		for (ch = 0; ch < CH_NUM; ++ch)	nof_received_records_all_ch += records_completed[ch];

		// *** Determine if collection is completed
		received_all_records = (nof_received_records_all_ch >= nof_records_all_ch);
		if (Save_Data_Enable) {
			fwrite(SaveBuffer, sizeof(short), buffer_cnt, outfile_data[0]);
			if (SHOW_STATUS_SAVING) {
					printf("Lines received: %u\r", nof_received_records_all_ch);
			}
		}
		buffer_cnt = 0;

	} while (!received_all_records);


	if (SHOW_STATUS_SAVING) {
		QueryPerformanceCounter((LARGE_INTEGER *)&ctr_finish);
		double elapsed_s = (ctr_finish - ctr_start) * period_s;
		size_t Total_Byte = ((size_t)nof_received_records_all_ch) * samples_per_record * 2; //2 bytes per sample
		double MeasuredDataRate = Total_Byte / (elapsed_s * 1024 * 1024);

		CHECKADQ(ADQ_GetWriteCountMax(adq_cu, adq_num, &retval));
		printf("\nNumber of Lines(records): %lu, Peak PCIe Transaction Size: %.3f MiB \n", nof_received_records_all_ch, Byte2MiB(retval * 128));
		printf("Stored %llu of %llu samples \n", SampleIndex, TotalSampNum);
		printf("Time elapsed: %.3f s, Data Received: %.3f GiB. Measured average transfer rate: %.3f MiB/s \n", elapsed_s, Byte2GiB(Total_Byte), MeasuredDataRate);
	}
	if (success) {
		//printf("Transfer Done!\n");
	}
	else {
		error:
			success = 0;
			EnableSegmentation = 0;
			printf("Error occurred in AcquireData().\n");
	}
	return success;

}


void adq7_triggered_streaming(void *adq_cu, int adq_num, int *argc, char **argv) {
	// Basic data saving settings
	const char outfile_mode[] = "wb";
	char filename[256];
	char FilePath[] = "D:/";
	int filename_index = 0;

	bool CreatedFiles = false; //No files are created on initial start up 
	int PressedKey = 0;

	QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
	period_s = 1.0 / freq; //micro second
	QueryPerformanceCounter((LARGE_INTEGER *)&ctr_init);

	//Setup ADQ
	adq_par.cu = adq_cu;
	adq_par.num = adq_num;

	const unsigned int max_nof_records = 0xffffffff;
	const unsigned int CheckPeriod = 8;
	int trig_mode;
	int trig_level;
	int trig_flank;
	unsigned int samples_per_record;
	unsigned int pretrig_samples;
	unsigned int holdoff_samples;
	unsigned int success;
	
	unsigned int trig_channel;
	unsigned int tr_buf_size = 9 * 512 * 1024;
	unsigned int tr_buf_no = 8;
	unsigned int trig_freq = 0;
	unsigned int trig_period = 0;
	
	unsigned int write_header_file = 0;
	unsigned int maskinput;

	char *serialnumber;
	unsigned int nof_channels;
	int exit = 0;
	unsigned int ch = 0;
	unsigned int i;

	printf("Laser_Clock = %.2f MHz, Target_ADC_Clock = %.3f MHz, Quarter_Line_Len = %d Samples, Actual_ADC_Clock (for PLL) = %.3f MHz, FPGA_Internal_Clock = %.3f MHz\n", Laser_Clock, Target_ADC_Clock, Quarter_Line_Len, Actual_ADC_Clock, FPGA_Clock);


	// Device details and FPGA status
	unsigned int tlocal = ADQ_GetTemperature(adq_cu, adq_num, 0) / 256;
	unsigned int tr1 = ADQ_GetTemperature(adq_cu, adq_num, 1) / 256;
	unsigned int tr2 = ADQ_GetTemperature(adq_cu, adq_num, 2) / 256;
	unsigned int tr3 = ADQ_GetTemperature(adq_cu, adq_num, 3) / 256;
	unsigned int tr4 = ADQ_GetTemperature(adq_cu, adq_num, 4) / 256;
	printf("Temperatures:\n\tLocal: %u\n\tADC0: %u\n\tADC1: %u\n\tFPGA: %u\n\tPCB diode: %u\n\n",
		tlocal, tr1, tr2, tr3, tr4);
	//serialnumber = ADQ_GetBoardSerialNumber(adq_cu, adq_num);
	//printf("Device Serial Number: %s\n", serialnumber);

	nof_channels = ADQ_GetNofChannels(adq_cu, adq_num);
	printf("Input channel count: %u\n", nof_channels);


	// Setup adjustable bias
	if (ADQ_HasAdjustableBias(adq_cu, adq_num)) {
		for (ch = 0; ch < nof_channels; ++ch) {
			success = ADQ_SetAdjustableBias(adq_cu, adq_num, ch + 1, analog_bias);
			if (success == 0)
				printf("Failed setting adjustable bias for channel %c.\n", "ABCD"[ch]);
			else
				printf("Adjustable bias for channel %c set to %d codes.\n", "ABCD"[ch], analog_bias);
		}
		printf("Waiting for bias settling...\n");
		Sleep(1000);
	}

	// *** Setup DBS
	unsigned int dbs_nof_inst = 0;
	unsigned char dbs_inst = 0;
	int dbs_bypass = 1;
	int dbs_dc_target = analog_bias;
	int dbs_lower_saturation_level = 0;
	int dbs_upper_saturation_level = 0;
	//ADQ_GetNofDBSInstances(adq_cu, adq_num, &dbs_nof_inst);
	//for (dbs_inst = 0; dbs_inst < dbs_nof_inst; ++dbs_inst) {
	//	printf("Setting up DBS instance %u ...\n", dbs_inst);
	//	success = ADQ_SetupDBS(adq_cu, adq_num,
	//		dbs_inst,
	//		dbs_bypass,
	//		dbs_dc_target,
	//		dbs_lower_saturation_level,
	//		dbs_upper_saturation_level);
	//	if (success == 0)
	//		printf("Failed setting up DBS instance %d.", dbs_inst);
	//}
	//Sleep(1000);


	unsigned int nofsamples_full = Quarter_Line_Len * 4; // 211 * 4; % 207 * 4; % Samples per line.FACED:125 * 4 = 500, AWG : 211 * 4 = 844, ATOM 207 * 4 = 828
	unsigned int internaltriggerperiod = nofsamples_full; //Only applicable when External clock source is used(clocksource = EXT)
	printf("Internal Trigger Period = %u Samples\n", internaltriggerperiod);

	//***************************************************
	CHECKADQ(ADQ_SetClockSource(adq_cu, adq_num, ADQ_CLOCK_EXT));
	trig_mode = ADQ_INTERNAL_TRIGGER_MODE;
	CHECKADQ(ADQ_SetTriggerMode(adq_cu, adq_num, trig_mode));
	trig_period = internaltriggerperiod;
	CHECKADQ(ADQ_SetInternalTriggerPeriod(adq_cu, adq_num, trig_period));

	// *** Set the number of lines (records) to acquire.
	nof_records = REC_LINE_NUM;

	// *** The number of samples per record.
	samples_per_record = REC_LINE_LEN;

	ImageSampNum = samples_per_record * nof_records;

	printf("RecNum: %d, RecLen: %d, Flush Timeout: %dms, Buffer size: %dbytes. Buffer Num: %d\n",
		nof_records, samples_per_record, timeout_ms, tr_buf_size, tr_buf_no);

	channelsmask = 0x00;
	maskinput = 1; //always enable the first available channel 
	if (maskinput > 0) channelsmask |= 0x01;

	if (nof_channels > 1) {
		printf("\nEnable channel B data collection? (0 or 1)\n");
		scanf("%d", &maskinput);
		if (maskinput > 0) channelsmask |= 0x02;
	}

	// Allocate memory
	for (ch = 0; ch < CH_NUM; ch++) {
		if (!((1 << ch) & channelsmask))
			continue;
		target_buffers[ch] = (short int *)malloc((size_t)tr_buf_size);
		if (!target_buffers[ch]) {
			printf("Failed to allocate memory for target_buffers\n");
			goto error;
		}
		target_headers[ch] = (StreamingHeader_t *)malloc((size_t)tr_buf_size);
		if (!target_headers[ch]) {
			printf("Failed to allocate memory for target_headers\n");
			goto error;
		}
		target_buffers_extradata[ch] = (short int *)malloc((size_t)(sizeof(short)*samples_per_record));
		if (!target_buffers_extradata[ch]) {
			printf("Failed to allocate memory for target_buffers_extradata\n");
			goto error;
		}
	}

	// Compute the sum of the number of records specified by the user
	nof_records_all_ch = 0; //clear value
	for (ch = 0; ch < CH_NUM; ++ch) {
		if (!((1 << ch) & channelsmask))
			continue;
		nof_records_all_ch += nof_records; //Total number of records from all channels
	}


	pretrig_samples = 0;
	holdoff_samples = 0;

	CHECKADQ(ADQ_SetTestPatternMode(adq_cu, adq_num, 0));

	// Use triggered streaming for data collection.
	CHECKADQ(ADQ_TriggeredStreamingSetup(adq_cu, adq_num,
		max_nof_records, //nof_records,
		samples_per_record,
		pretrig_samples,
		holdoff_samples,
		channelsmask));

	// Commands to start the triggered streaming mode after setup
	CHECKADQ(ADQ_ResetWriteCountMax(adq_cu, adq_num));
	CHECKADQ(ADQ_SetStreamStatus(adq_cu, adq_num, 1));
	CHECKADQ(ADQ_SetTransferBuffers(adq_cu, adq_num, tr_buf_no, tr_buf_size));
	CHECKADQ(ADQ_StopStreaming(adq_cu, adq_num));

	//************* Set cell trigger parameters **************************************

	unsigned int par_val;
	
	CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 1, 16, 0, 0, &par_val));
	CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 1, 17, 0, 0, &par_val));
	CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 1, 18, 0, 0, &par_val));
	CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 1, 19, 0, CheckPeriod, &par_val));

	CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 16, 0, 1, &par_val)); //synchronous clear for FPGA main acquisition process
	CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 16, 0, CellParam, &par_val)); //update Cell parameter settings

	CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 17, 0, CellDetectTh << 16 | Diff_X_offset << 8 | CellMargin, &par_val)); //update cell trigger/seqmentation settings
	CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 18, 0, ((unsigned int)1) << 31, &par_val)); //Reset data acquisition and line count
	CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 19, 0, ((unsigned int)0) << 31, &par_val)); //disable debug acquisition

	CHECKADQ(ADQ_StartStreaming(adq_cu, adq_num));
	// When StartStreaming is issued, device is armed and ready to accept triggers

//***Initialise Data Acqusision counters***************************************************************************************************************
	
	do {
		nof_records = REC_LINE_NUM;
		nof_records_all_ch = 0; //clear value
		for (ch = 0; ch < CH_NUM; ++ch) {
			if (!((1 << ch) & channelsmask)) continue;
			nof_records_all_ch += nof_records; //Total number of records from all channels
		}

  //******* OpenGL Display output **********************
	
		Start_Save = false;
		WithinGLUTloop = true;
		GL_main(argc, argv);
		WithinGLUTloop = false;
		
		if  (Start_Save) {
			//EnableMenuItem(hmenu, SC_CLOSE, MF_ENABLED);
			printf("\nChoose the number of lines (records) to acquire (0=cancel):\n");
			scanf("%llu", &nof_records);
			if (nof_records==0) break;
			nof_records_all_ch = 0; //clear value
			for (ch = 0; ch < CH_NUM; ++ch) {
				if (!((1 << ch) & channelsmask)) continue;
				nof_records_all_ch += nof_records; //Total number of records from all channels
			}
			//EnableMenuItem(hmenu, SC_CLOSE, MF_GRAYED); //Stop user from closing the console window
			TotalSampNum = samples_per_record * nof_records;

			printf("RecNum: %d, nof_records_all_ch: %d, Flush Timeout: %dms\n",	nof_records,nof_records_all_ch, timeout_ms);
			for (ch = 0; ch < nof_channels; ++ch) {
				if (!((1 << ch) & channelsmask)) continue;
				// Open output files for headers
				do{
					sprintf(filename, "%sData%c%s%s%d.out", FilePath, "ABDC"[ch], SysName, "_Result",filename_index);
					++filename_index;
				}while (file_exists(filename));

				outfile_data[ch] = fopen(filename, outfile_mode);
				if (!outfile_data[ch]) {
					printf("Failed to open output file %s.\n", filename);
					goto error;
				} else {
					printf("Writing data to file: \"%s\".\n", filename);
				}

			}
			CreatedFiles = true;

			SHOW_STATUS_SAVING = true;
			 //save data to file
			int success = AcquireData(true);

			SHOW_STATUS_SAVING = false;
			for(ch = 0; ch < CH_NUM; ch++) {
				if (!((1<<ch) & channelsmask)) continue;
				if (outfile_data[ch]) fclose(outfile_data[ch]);

			}
			if (!success) break;
			else PressedKey = ConsolePause ("Press Any Key to continue or Esc to exit.");

		}
  	
	 } while (Start_Save && (PressedKey!=27));

	
  //****************************************************
	
 error:
  TrigShift = 0;
  WithinGLUTloop = false;
  CHECKADQ(ADQ_StopStreaming(adq_cu, adq_num));

  CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 16, 0, 1, &par_val));
  CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 16, 0, 0, &par_val));
  CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 17, 0, 0, &par_val));
  CHECKADQ(ADQ_WriteUserRegister(adq_cu, adq_num, 2, 18, 0, ((unsigned int)1) << 31, &par_val)); //Reset data acquisition and line count

  for(ch = 0; ch < CH_NUM; ch++) {
    if (target_buffers[ch])
      free(target_buffers[ch]);
    if (target_headers[ch])
      free(target_headers[ch]);
    if (target_buffers_extradata[ch])
      free(target_buffers_extradata[ch]);
  }

  // Close any open output files
  if (CreatedFiles) {
    for(ch = 0; ch < CH_NUM; ch++) {
      if (!((1<<ch) & channelsmask)) continue;
      if (outfile_data[ch]) fclose(outfile_data[ch]);
      //if (outfile_headers[ch]) fclose(outfile_headers[ch]);
    }
  }

  return;
}

//********************************************

static BOOL WINAPI console_ctrl_handler(DWORD dwCtrlType) // Handler function will be called on separate thread!
{
  switch (dwCtrlType)
  {
  case CTRL_C_EVENT: // Ctrl+C
    break;
  case CTRL_BREAK_EVENT: // Ctrl+Break
    break;
  case CTRL_CLOSE_EVENT: // Closing the console window
	  printf("Press CLOSE button.\n");
	  return FALSE;
    break;
  case CTRL_LOGOFF_EVENT: // User logs off. Passed only to services!
    break;
  case CTRL_SHUTDOWN_EVENT: // System is shutting down. Passed only to services!
    break;
  }

  // Return TRUE if handled this message, further handler functions won't be called.
  // Return FALSE to pass this message to further handlers until default handler calls ExitProcess().
  return FALSE;
}


void SetVSync(BOOL sync)
{
	typedef BOOL(APIENTRY *PFNWGLSWAPINTERVALPROC)(int);
	PFNWGLSWAPINTERVALPROC wglSwapIntervalEXT = 0;

	const char *extensions = (char*)glGetString(GL_EXTENSIONS);
	wglSwapIntervalEXT = (PFNWGLSWAPINTERVALPROC)wglGetProcAddress("wglSwapIntervalEXT");
	if (wglSwapIntervalEXT)	wglSwapIntervalEXT(sync);
}


void ScaleImage(const int SampNum, GLshort* ImageIn, GLubyte* ImageOut, ImgInfo_t DataInfo, const bool IsInvertSignal) {
	const int offset = IsInvertSignal ? (DataInfo.Max) : DataInfo.Min;
	const float Scaler = IsInvertSignal ? (-DataInfo.Scale) : DataInfo.Scale;

	for (int i = 0; i < SampNum; i++) { //ImageSampNum
		int FullRangeVal = (((int)ImageIn[i]) - offset)*Scaler;
		ImageOut[i] = (FullRangeVal<0) ? 0u : FullRangeVal;
		//ImageOut[i] = ((((int)ImageIn[i]) - DataInfo.Min)<0) ? 0 : (((int)ImageIn[i]) - DataInfo.Min)*DataInfo.Scale;
	}
}
void ScaleImageRGB(const int SampNum, GLshort* ImageIn, GLubyte* ImageOut, ImgInfo_t DataInfo, const bool IsInvertSignal) {
	const int offset = IsInvertSignal ? (DataInfo.Max) : DataInfo.Min;
	const float Scaler = IsInvertSignal ? (-DataInfo.Scale) : DataInfo.Scale;

	for (int i = 0; i < SampNum; i++) { //ImageSampNum
		int FullRangeVal = (((int)ImageIn[i]) - offset)*Scaler;
		if (FullRangeVal < 0) {
			ImageOut[3*i] = 0u; ImageOut[3*i+1] = 0u; ImageOut[3*i+2] = 0u;
		} else if (FullRangeVal > 255){
			ImageOut[3 * i] = 255u; ImageOut[3 * i + 1] = 255u; ImageOut[3 * i + 2] = 255u;
		} else {
			ImageOut[3*i] = FullRangeVal; ImageOut[3*i+1] = FullRangeVal; ImageOut[3*i+2] = FullRangeVal;
		}
	}
}

ImgInfo_t GetImageScale(int target_range, int max_margin, int min_margin, bool UsePrev, bool verbose) {
	int i;
	static ImgInfo_t DataInfo;
	if (!UsePrev) {
		DataInfo.Max = -32768;
		DataInfo.Min = 32767;
	}
	for (i = 0; i < ImageSampNum; i++) {
		if (mono16Image1D[i]>DataInfo.Max) DataInfo.Max = mono16Image1D[i];
		if (mono16Image1D[i]<DataInfo.Min) DataInfo.Min = mono16Image1D[i];
	}

	if (!UsePrev) {
		DataInfo.Max += max_margin;
		DataInfo.Min -= min_margin;
	}
	DataInfo.Range = DataInfo.Max - DataInfo.Min;
	DataInfo.Scale = (float)target_range / DataInfo.Range;
	if (verbose) printf("Image Max: %d, Min: %d, data_range: %u, Scale factor (0-%u): %.6f\n", DataInfo.Max, DataInfo.Min, DataInfo.Range, target_range, DataInfo.Scale);
	
	return DataInfo;
}


void GetBG16(GLshort* Image, GLshort* OutBGLine, GLshort* OutBGLineH) {
	for (int j = 0; j < (REC_LINE_LEN); j++) {
		int val =0;
		for (int i = 0; i < REC_LINE_NUM; ++i) {
			const int n = i * REC_LINE_LEN + j;
			val+= Image[n];
		}
		OutBGLine[j] = val/REC_LINE_NUM;
	}

	const int mean_num = 16;
	for (int i = 0; i < REC_LINE_NUM; ++i) { //across lines (flow direction)
		int val =0;
		for (int j = 0; j < mean_num; j++) {
			const int n = i * REC_LINE_LEN + j;
			val+= Image[n];
		}
		OutBGLineH[i] = val/mean_num;
	}

}

void RemoveBG16(GLshort* Image,  GLshort* OutBGLine, GLshort* OutBGLineH, const int mode) {

	for (int i = 0; i < REC_LINE_NUM; i++) {
		for (int j = 0; j < REC_LINE_LEN; j++) {
			const int n = i * REC_LINE_LEN + j;
			const int m = i * REC_LINE_LEN + (REC_LINE_LEN-REC_LINE_LEN/8);
			Image[n] = (Image[n]-OutBGLine[j])-OutBGLineH[i];
			//Image[n] = OutBGLine[j]; 
		}
	}

}
void ModePicker16(GLshort* Image, const GLubyte PeakDist, const bool IsInvertSignal) { //const int LineNum, const int LineLen, 
		for (int i = 0; i < REC_LINE_NUM; ++i) {
			for (int j = 0; j < (REC_LINE_LEN/PeakDist); j++) {
				const int read_i = i * REC_LINE_LEN + j*PeakDist;
				const int write_i = i * REC_LINE_LEN + j;
				GLshort val = Image[read_i];
				for (int k = 1; (k < PeakDist) && ((j + k) < REC_LINE_LEN); k++) {
					max16(val, Image[read_i + k]);
				}
				Image[write_i] = val;
			}
		}

}

// *** Scrolling Cell image display *************************************************************************************************
#define yTop 0.75f //1.0
#define yBottom 0.25f //-1.0
#define xRight 0.0f //2.0
#define xLeft  -1.0f //-2.0

const float ViewBottom = 0.0f, ViewTop = 1.0f;
const float BaseTop = 1.0 - 1.0/PeakDist;
const float ViewBottomPM = BaseTop+(ViewBottom/PeakDist), ViewTopPM = BaseTop+(ViewTop/PeakDist); //for full vertical view//

const GLsizei TexWidth = REC_LINE_LEN;
const GLsizei TexHalfHeight = REC_LINE_NUM;
const GLsizei TexHeight = REC_LINE_NUM * 2;

#define ColourByteCount 3
const GLsizei TexPixNumHalf = TexWidth * TexHalfHeight;
const GLsizei TexArraySizeHalf = TexPixNumHalf * ColourByteCount;
const GLsizei TexArraySize = TexArraySizeHalf * 2;

GLubyte ScaledRGB8Image1D[TexArraySizeHalf] = { 0 };
GLubyte ScaledGrey8Image1D[TexPixNumHalf] = { 0 };

const GLsizei mipmap_lv = 5; //valid range: 1-5
const GLfloat anisotropy = 16.0f;

const GLfloat DeltaX = ((1.0f) / (ImageHeight));
GLuint StepSize = 16;
GLuint TotalStepNum = ((StepSize >= 1U) ? (((GLuint)(ImageHeight)) / StepSize) : 1U);
GLfloat DeltaStep = DeltaX * StepSize;
GLbyte ScrollSpeedChange = 0;
DWORD ExtraFrameDelay = 160;

//GLubyte TexSel = 1;
GLuint TextureID;

void ChangeScrollSpeed(const bool increase) {
	if (increase) {
		StepSize = (StepSize == 0) ? 1 : (StepSize * 2);
		if (StepSize >= ImageHeight) StepSize = 0;
		printf("Increase scrolling Step Size: %u\n", StepSize);
	} else {
		StepSize = (StepSize == 0) ? (ImageHeight/2) : (StepSize/2);
		printf("Reduce scrolling Step Size: %u\n", StepSize);
	}
	TotalStepNum = ((StepSize >= 1U) ? (((GLuint)(ImageHeight)) / StepSize) : 1U);
	DeltaStep = DeltaX * StepSize;
}

void GL_reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(xRight, -xLeft, 0.0, 1.0, -1.0, 1.0);
	//glOrtho(0.125, 0.875, 0.0, 1.0, -1.0, 1.0);
	//glOrtho(-1.1, 1.1, 0.0, 1.0, -10.0, 10.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	printf("w: %.3f, h: %.3f\r", (float)FrameWidth / w, (float)FrameHeight / h);
}

GLfloat fShiftA = 0.0f;
GLfloat fShiftB = 1.0f; //DeltaX * StepSize * TotalStepNum; //This equals 1.0 !!
GLuint StepCount = 0;
GLfloat fShift = 0.0f;
GLboolean ShiftSel = 0;
ImgInfo_t DataInfo;
GLboolean EnableRescale = 0;
GLboolean EnableModeSel = 0;
GLboolean RemoveBG = 0;
GLboolean RemoveBGChanged = 0;

GLboolean ImgParamChanged = 0;

void init_tex(void) {
	printf("Texture Resolution: %u(W) x %u(H)\n", TexWidth, TexHalfHeight);

	SetVSync(vsync);
	glClearColor(0.0, 0.0, 0.2, 0.0);
	glShadeModel(GL_FLAT);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glGenTextures(1, &TextureID);   // generate a texture handler really reccomanded (mandatory in openGL 3.0)
	glBindTexture(GL_TEXTURE_2D, TextureID); // tell openGL that we are using the texture 
	glTexStorage2D(GL_TEXTURE_2D, mipmap_lv, GL_RGB8, TexWidth, TexHeight);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //GL_CLAMP_TO_EDGE, GL_MIRRORED_REPEAT, GL_REPEAT //Horizontal direction
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); //GL_CLAMP_TO_EDGE, GL_MIRRORED_REPEAT, GL_REPEAT //Vertical direction

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);//texture magnify filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); //texture filtering
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropy);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, -1000.0f);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, 1000.0f);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, 0.2f);


	CellParam = IncludeLastCellNext << 10 | BackgroundSeg << 9 | UseCellDetect << 8 | UseDelayedStart << 7 | DebugDetect << 6 | 0 << 5 | UL2_Mode << 1;
	CellParamChanged = true;

	if (!AcquireData()) glutLeaveMainLoop(); //Get image data from ADQ7 FPGA //Get image data from ADQ7 FPGA
	DataInfo = GetImageScale(255, 1000, 0, true, true);
	if (DataInfo.Min < (-6000)) DataInfo.Min = -6000;

	EnableRescale = 0;

	ScaleImageRGB(ImageSampNum, mono16Image1D, ScaledRGB8Image1D, DataInfo, InvertSignal);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, TexWidth, TexHalfHeight, GL_RGB, GL_UNSIGNED_BYTE, ScaledRGB8Image1D);

	glGenerateMipmap(GL_TEXTURE_2D);  //Generate num_mipmaps number of mipmaps here.

	fShiftA = 0.0f;
	fShiftB = 1.0f;
	StepCount = 0;
	fShift = 0.0f;
	ShiftSel = 0;


	CellParam = IncludeLastCellNext << 10 | BackgroundSeg << 9 | UseCellDetect << 8 | UseDelayedStart << 7 | DebugDetect << 6 | EnableSegmentation << 5 | UL2_Mode << 1;
	CellParamChanged = true;

}

inline void DrawTexturedRect(const GLfloat xL, const GLfloat xR, const GLfloat yT, const GLfloat yB, const GLfloat xmove, const GLboolean PartSel, const GLuint texID) {
	const GLfloat t0 = !PartSel ? 0.0f : 0.5f; //0
	const GLfloat t1 = !PartSel ? 0.5f : 1.0f; //1

	glBegin(GL_TRIANGLE_STRIP);  // draw image as texture on triangle strip

	//*** Texture is rotated clockwise by 90 degrees when mapping to the rect's corners
	//*** so the texture-space s(horizontal axis) and t(vertical axis) are also rotated by 90 degrees clockwise when mapping to the model-space coordinates
	//*** Therefore, TexCoord: s -> reversed-y, t -> reversed-x
	glTexCoord2f(1.0, t1); glVertex2f(xL + xmove, yB); //[1]
	glTexCoord2f(0.0, t1); glVertex2f(xL + xmove, yT); //[2] // + fshift
	glTexCoord2f(1.0, t0); glVertex2f(xR + xmove, yB); //[3]
	glTexCoord2f(0.0, t0); glVertex2f(xR + xmove, yT); //[4]
	glEnd();

	// Vertex order and index
	// [2]--[4]
	//  | \  |
	//  |  \ |
	// [1]--[3]

	// Regular Texture clamp indices
	// [4(0,0)]-[3(0,1)]
	//  |           |
	//  |           |
	//  |           |
	// [2(1,0)]-[1(1,1)]
}



inline void ProcessSamples(void) {
	if (EnableModeSel) ModePicker16(mono16Image1D, PeakDist, InvertSignal);
	if (RemoveBG) {
		GetBG16(mono16Image1D, mono16ImageBGLine,mono16ImageBGLineH);
		RemoveBG16(mono16Image1D,  mono16ImageBGLine, mono16ImageBGLineH, 0);
	}
	if (EnableRescale || (ImgParamChanged && RemoveBGChanged)) {
		RemoveBGChanged = 0;
		DataInfo = GetImageScale(255, 0, 0, false, false);
		if (DataInfo.Min < -6000) DataInfo.Min = -6000;
	}
	ScaleImageRGB(ImageSampNum, mono16Image1D, ScaledRGB8Image1D, DataInfo, InvertSignal);
}

inline void SwitchModePickView(void) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (EnableModeSel) {
		glOrtho(xRight, -xLeft, ViewBottomPM, ViewTopPM, -1.0, 1.0);
	} else {
		glOrtho(xRight, -xLeft, ViewBottom, ViewTop, -1.0, 1.0);
	}
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void draw_tex(void)
{

	SwitchModePickView();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, TextureID);

	DrawTexturedRect(xLeft, xRight, 1.0f, 0.0f, fShiftA, 1, TextureID);
	DrawTexturedRect(xLeft, xRight, 1.0f, 0.0f, fShiftB, 0, TextureID);

	if (RunMode == 1) {

		if (ScrollSpeedChange) {
			ChangeScrollSpeed(ScrollSpeedChange==1); // 0 =slow down, 1 = speed up to the right
			ScrollSpeedChange = 0;
		}

		fShift = DeltaStep * StepCount;
		//printf("wShift: %u, FrameCount: %u\r", wShift, FrameCount);

		if (!StepCount || TriggerPosChanged || ImgParamChanged) {
			
			if (!AcquireData()) glutLeaveMainLoop(); //Get image data from ADQ7 FPGA
			ProcessSamples();
			ScaleImageRGB(ImageSampNum, mono16Image1D, ScaledRGB8Image1D, DataInfo, InvertSignal);
			glTextureSubImage2D(TextureID, 0, 0, (!ShiftSel) ? (TexHalfHeight) : 0, TexWidth, TexHalfHeight, GL_RGB, GL_UNSIGNED_BYTE, ScaledRGB8Image1D);

			if (TriggerPosChanged || ImgParamChanged) {
				StepCount = 0;
				TriggerPosChanged = false;
				ImgParamChanged = 0;
				if (!AcquireData()) glutLeaveMainLoop();//Get image data from ADQ7 FPGA
				ProcessSamples();
				glTextureSubImage2D(TextureID, 0, 0, (ShiftSel) ? (TexHalfHeight) : 0, TexWidth, TexHalfHeight, GL_RGB, GL_UNSIGNED_BYTE, ScaledRGB8Image1D);

			} else {
				ShiftSel = !ShiftSel;
			}
			glGenerateMipmap(GL_TEXTURE_2D);
		}

		if (ShiftSel) {
			fShiftA = fShift;
			fShiftB = fShift + 1.0f;
		}
		else {
			fShiftA = fShift + 1.0f;
			fShiftB = fShift;
		}

		//*******************************

		StepCount = (StepCount < (TotalStepNum - 1)) ? (StepCount + 1u) : 0u;

		GLuint par_val;
		ADQ_ReadUserRegister(adq_par.cu, adq_par.num, 2, 18, &par_val);
		double CellRate = (double)par_val*CellCountConversionFactor;
		printf("Rate: %.2f cells/s   \r", CellRate);

		glutPostRedisplay();
	}

	glutSwapBuffers();
	if (!StepSize) Sleep(ExtraFrameDelay);
}



void GL_main(int *argc, char **argv)
{
	const int WinWidth = FrameWidth/2;
	const int WinHeight = FrameHeight/2;
	printf("\nInitialising GL window.\n");
	glutInit(argc, argv);

	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowPosition(100, 25);//optional
	glutInitWindowSize(WinWidth, WinHeight); //optional
	int hWin = glutCreateWindow("Raw Image Window");

	glewInit();
	// *** Check GL versions and capabilities **********
	if (glewIsSupported("GL_VERSION_4_6"))
	{
		printf(" GLEW Version is 4.6\n");
	}
	else
	{
		printf("GLEW 4.6 not supported\n");
	}
	if (glewIsSupported("GL_EXT_texture_filter_anisotropic")) printf(" GL_EXT_texture_filter_anisotropic is supported.\n");
	// Set Flag that extension is supported
	GLfloat fLargest;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
	printf(" GL_MAX_TEXTURE_MAX_ANISOTROPY: %.3f.\n", fLargest);

	// *** main function calls **********
	init_tex();
	glEnable(GL_DEPTH_TEST);
	glutReshapeFunc(GL_reshape);
	glutDisplayFunc(draw_tex);

	glutKeyboardFunc(keyboard);
	glutSpecialFunc(SpecialInputDn);
	glutSpecialUpFunc(SpecialInputUp);

	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
	glutCloseFunc(CloseFn);
	glutMainLoop();
}

void SpecialInputDn(int key, int x, int y)
{
	void *adq_cu = adq_par.cu;
	int adq_num = adq_par.num;
	unsigned int retval;

	switch (key)
	{
	case GLUT_KEY_UP:
		if (!shift_key_dn) {
			TrigShift += 32;
		} else {
			TrigShift++;
		}
		TrigShift = TrigShift % ((GLuint)REC_LINE_LEN);
		TrigCoarseTune = TrigShift / 32u;
		TrigFineTune = TrigShift % 32u;

		printf("Trigger +, coarse: %u, Fine: %u, TrigShift: %u\n", TrigCoarseTune, TrigFineTune, TrigShift);
		ADQ_WriteUserRegister(adq_cu, adq_num, 1, 18, 0, TrigShift, &retval);
		TriggerPosChanged = true;
		//printf("Key UP: %u\n", key);
		break;
	case GLUT_KEY_DOWN:
		if (!shift_key_dn) {
			TrigShift = (TrigShift>=32u)? (TrigShift-32u) : (TrigShift+REC_LINE_LEN-32u);
		} else {
			TrigShift = (TrigShift > 0) ? (TrigShift - 1u) : (REC_LINE_LEN - 1u);
		}
		TrigCoarseTune = TrigShift / 32u;
		TrigFineTune = TrigShift % 32u;
		
		printf("Trigger -, coarse: %u, Fine: %u, TrigShift: %u\n", TrigCoarseTune, TrigFineTune, TrigShift);
		ADQ_WriteUserRegister(adq_cu, adq_num, 1, 18, 0, TrigShift, &retval);
		TriggerPosChanged = true;
		//printf("Key DOWN: %u\n", key);
		break;
	case GLUT_KEY_LEFT:
		//ChangeScrollSpeed(0);
		ScrollSpeedChange = -1;
		//printf("Key LEFT: %u\n", key);
		break;
	case GLUT_KEY_RIGHT:
		//ChangeScrollSpeed(1);
		ScrollSpeedChange = 1;
		//printf("Key RIGHT: %u\n", key);
		break;
	case KEY_SHIFT:
		//do something here
		shift_key_dn = true;
		//printf("Key SHIFT down: %u\n", key);
		break;
	case KEY_CTRL:
		//do something here
		ctrl_key_dn = true;
		printf("\nKey CTRL down: %u\n", key);
		break;
	default:
		printf("\nSpecial Key: %u\n", key);
		break;
	}
}

void SpecialInputUp(int key, int x, int y)
{
	switch (key)
	{
	//case GLUT_KEY_UP:
	//	//do something here
	//	printf("Key UP: %u\n", key);
	//	break;
	//case GLUT_KEY_DOWN:
	//	//do something here
	//	printf("Key DOWN: %u\n", key);
	//	break;
	//case GLUT_KEY_LEFT:
	//	//do something here
	//	printf("Key LEFT: %u\n", key);
	//	break;
	//case GLUT_KEY_RIGHT:
	//	//do something here
	//	printf("Key RIGHT: %u\n", key);
	//	break;
	case KEY_SHIFT:
		//do something here
		shift_key_dn = false;
		//printf("Key SHIFT up: %u\n", key);
		break;
	case KEY_CTRL:
		//do something here
		ctrl_key_dn = false;
		printf("Key CTRL up: %u\n", key);
		break;
	//default:
	//	printf("Special Key: %u\n", key);
	//	break;
	}
}
void keyboard(unsigned char key, int x, int y)
{
	
	switch (key) {
	case 'm': //Enable mode select
		EnableModeSel = !EnableModeSel;// Toggle to opposite value
		ImgParamChanged = 1;
		printf("Enable Mode Select : %u\n", EnableModeSel);
		break;
	case 'n': //Enable BG removal
		RemoveBG = !RemoveBG;
		RemoveBGChanged = 1;
		ImgParamChanged = 1;
		printf("Enable BG removal : %u\n", RemoveBG);
		break;
	case 'p':
		RunMode = 1 - RunMode;      // Toggle to opposite value
		printf("RunMode: %d\n", RunMode);
		if (RunMode == 1) {
			glutPostRedisplay();
		}
		else {
			printf("\n");
		}
		break;
	case 's': //rescale image colour range
		EnableRescale = !EnableRescale;// Toggle to opposite value
		printf("Rescale Image colour range: %u\n", EnableRescale);

		break;
	case 'v':
		vsync = 1 - vsync; // Toggle to opposite value
		printf("Vertical Sync: %d\n", vsync);
		SetVSync(vsync);
		break;
	case ']':
		if (CellMargin < MaxCellMargin) CellMargin = CellMargin + 1;
		SegmentationParam = CellDetectTh << 16 | Diff_X_offset << 8 | CellMargin;
		SegParamChanged = true;
		printf("Cell Margin +1, Margin = %d\n",CellMargin);
		ImgParamChanged = 1;
		break;
	case '[':
		if (CellMargin > 1) CellMargin = CellMargin - 1;
		SegmentationParam = CellDetectTh << 16 | Diff_X_offset << 8 | CellMargin;
		SegParamChanged = true;
		printf("Cell Margin -1, Margin = %d\n",CellMargin);
		ImgParamChanged = 1;
		break;  
	case '\'':
		if (Diff_X_offset < MaxDiffOffset) Diff_X_offset = Diff_X_offset + 1;
		SegmentationParam = CellDetectTh << 16 | Diff_X_offset << 8 | CellMargin;
		SegParamChanged = true;
		printf("Diff_X_offset +1, offset = %d (Upper Limit: %d)\n",Diff_X_offset, MaxDiffOffset); //MaxDiffOffsetCtrlLimit
		ImgParamChanged = 1;
		break;
	case ';':
		if (Diff_X_offset > 2) Diff_X_offset = Diff_X_offset - 1;
		SegmentationParam = CellDetectTh << 16 | Diff_X_offset << 8 | CellMargin;
		SegParamChanged = true;
		printf("Diff_X_offset -1, offset = %d (Lower Limit: %d)\n",Diff_X_offset, 2); //MaxDiffOffsetCtrlLimit
		ImgParamChanged = 1;
		break;
	case '.':
		if (CellDetectTh<=(10000-10)) CellDetectTh = CellDetectTh + 10;
		SegmentationParam = CellDetectTh << 16 | Diff_X_offset << 8 | CellMargin;
		SegParamChanged = true;
		printf("Cell Detect Threshold +10, Threshold = %d\n",CellDetectTh);
		ImgParamChanged = 1;
		break;
	case ',':
		if (CellDetectTh>=10) CellDetectTh = CellDetectTh - 10;
		SegmentationParam = CellDetectTh << 16 | Diff_X_offset << 8 | CellMargin;
		SegParamChanged = true;
		printf("Cell Detect Threshold -10, Threshold = %d\n",CellDetectTh);
		ImgParamChanged = 1;
		break;
	case 'z':
		EnableSegmentation = !EnableSegmentation;
		CellParam = IncludeLastCellNext << 10 | BackgroundSeg << 9 | UseCellDetect << 8 | UseDelayedStart << 7 | DebugDetect << 6 | EnableSegmentation << 5 | UL2_Mode << 1;
		printf("Enable Segmentation: %d\n", EnableSegmentation);
		CellParamChanged = true;
		ImgParamChanged = 1;
		break;
	case 13: //Enter
		printf("Step 1 frame\n", RunMode);
		RunMode = 1;
		draw_tex();
		//drawImage();
		RunMode = 0;
		break;
	case 49: //NumPad 1
		printf("Begin saving Data.\n", RunMode);
		Start_Save = true;
		glutLeaveMainLoop();
		break;
	case '-'://61: //NumPad 1
		ExtraFrameDelay= (ExtraFrameDelay<500)?(ExtraFrameDelay+10):500;
		printf("Reducing Frame rate (Frame time = %d ms)\n", ExtraFrameDelay);

		break;
	case '='://45: //NumPad 1
		ExtraFrameDelay= (ExtraFrameDelay>0)?(ExtraFrameDelay-10):0;
		printf("Increasing Frame rate (Frame time = %d ms)\n", ExtraFrameDelay);

		break;
	case 27: //Escape
		//exit(0);
		printf("Exiting GL Main Loop.\n");
		glutLeaveMainLoop(); // return to main function instead of terminating completely
	default: 
		printf("\nKey pressed: %u\n", key);
	}
}
void CloseFn(void) {
	printf("\nGL Window and Main Loop terminated.\n");
}